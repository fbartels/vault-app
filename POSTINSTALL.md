On first visit, you can generate the keys for the vault. The keys are not stored
in the app and without the keys you cannot unseal the vault.

To enable authentication against the Cloudron user directory, unseal the vault and
run `/app/pkg/enable-ldap.sh <root-token>` using the
[Web Terminal](https://cloudron.io/documentation/apps/#web-terminal).
